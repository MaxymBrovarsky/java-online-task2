package com.brom.rumors;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Main class of program
 * @author Maxym Brovarsky
 * @version 1.0
 */
public class RumorsApplication {
    private static final String ENTER_GUEST_MESSAGE = "Hi. Please enter amount of guests.";
    private static final String WRONG_INPUT_MESSAGE = "Wrong input. Please enter INTEGER.";
    private static final String HEARD_RUMOR_MESSAGE = "Rumor was heard by guest with id: ";
    private static final String AMOUNT_OF_GUEST_THAT_HEARD_RUMOR_MESSAGE = " guests heard rumor(exlude BOB).";
    private static final String RUMOR_START_MESSAGE = "BOB start rumor";
    private static final int BOB_ID = 0;
    private static final int INITIAL_PROBABILITY = 1;

    public static void main(String[] args) {
        RumorsApplication rumorsApplication = new RumorsApplication();
        rumorsApplication.run();
    }

    /**
    *  This method is a entry point of program
     */
    public void run() {
        int amountOfGuests = readAmountOfGuests();
        int amountOfGuestsThatHeardRumor = calculateAmountOfGuestsThatHeardRumor(amountOfGuests);
        printAmountOfPeopleThatHeardRumor(amountOfGuestsThatHeardRumor);
        double probability = calculateProbabilityThatAllGuestsWillHearRumor(amountOfGuests);
        printProbabilityThatAllHeardRumor(probability);

    }
    /**
     *This method read amount of guests
     * @return amount of guests
     */
    public int readAmountOfGuests() {
        int amountOfGuests = 0;
        System.out.println(ENTER_GUEST_MESSAGE);
        Scanner scanner = new Scanner(System.in);
        boolean valueIsInteger = false;
        while (!valueIsInteger) {
            if (scanner.hasNextInt()) {
                amountOfGuests = scanner.nextInt();
                valueIsInteger = true;
            } else {
                System.out.println(WRONG_INPUT_MESSAGE);
                scanner.nextLine();
            }
        }
        return amountOfGuests;
    }

    /**
     * This method calculate probability that all guests at party will hear a rumor about Alice
     * @param amountOfGuests number of guests at party
     * @return double value of probability
     */
    public double calculateProbabilityThatAllGuestsWillHearRumor(int amountOfGuests) {
        double probability = INITIAL_PROBABILITY;
        for (int i = 0; i < amountOfGuests; ++i) {
            probability = probability * (amountOfGuests - i) / (double) amountOfGuests;
        }
        return probability;
    }

    /**
     * This method calculate amount of guests that will hear rumor, before it will stop propagation
     * @param amountOfGuests number of guests at party
     * @return amount of user
     */
    public int calculateAmountOfGuestsThatHeardRumor(int amountOfGuests) {
        int lastHeardId = BOB_ID;
        System.out.println(RUMOR_START_MESSAGE);
        int amountOfGuestsThatHeardRumor = amountOfGuests;
        Map<Integer, Boolean> idToHeard = initializeIdToHeardMap(amountOfGuests);
        idToHeard.put(lastHeardId, true);
        Random random = new Random();
        for (int i = 0; i < amountOfGuests; ++i) {
            int nextHeardId = this.getNextHeardId(random, lastHeardId, amountOfGuests);
            System.out.println(HEARD_RUMOR_MESSAGE + nextHeardId);
            if (idToHeard.get(nextHeardId)) {
                amountOfGuestsThatHeardRumor = i;
                break;
            } else {

                idToHeard.put(nextHeardId, true);
                lastHeardId = nextHeardId;
            }
        }
        return amountOfGuestsThatHeardRumor;
    }

    /**
     * This method returd id of guest that will hear rumor next
     * @param random instance of java.util.Random
     * @param lastHeardId id of guest that heard rumor last
     * @param amountOfGuests number of guests at party
     * @return id of guest that will hear rumor
     */
    public int getNextHeardId(Random random, int lastHeardId, int amountOfGuests) {
        int nextHeardId;
        boolean getNewGuestForHearingFlag = false;
        do {
            nextHeardId = random.nextInt(amountOfGuests);
            if (nextHeardId != lastHeardId) {
                getNewGuestForHearingFlag = true;
            }
        } while (!getNewGuestForHearingFlag);
        return nextHeardId;
    }

    private Map<Integer, Boolean> initializeIdToHeardMap(int size) {
        Map<Integer, Boolean> idToHeard = new HashMap<Integer, Boolean>();
        for (int i = 0; i < size; ++i) {
            idToHeard.put(i, false);
        }
        return idToHeard;
    }

    /**
     * print amout of guests that heard rumor
     * @param amountOfGuestsThatHeardRumor number of guests that heard rumor
     */
    private void printAmountOfPeopleThatHeardRumor(int amountOfGuestsThatHeardRumor) {
        System.out.println(amountOfGuestsThatHeardRumor + AMOUNT_OF_GUEST_THAT_HEARD_RUMOR_MESSAGE);
    }

    /**
     * print value of probability(in percents) that all guests will hear rumor
     * @param probabilityThatAllHeardRumor probability value that everyone at party will hear rumor
     */
    private void printProbabilityThatAllHeardRumor(double probabilityThatAllHeardRumor) {
        double probabilityThatAllHeardRumorInPercents = probabilityThatAllHeardRumor * 100;
        System.out.printf("Probability that all will hear rumor equal %14.10f%%", probabilityThatAllHeardRumorInPercents);
    }


}
